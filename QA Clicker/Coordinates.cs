﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QA_Clicker
{
    public class Coordinates
    {
        public static async void GetCoordinates(TextBox textBox1, TextBox textBox2)
        {
            while (true)
            {
                await Task.Delay(200);
                var xx = Cursor.Position.X.ToString();
                var yy = Cursor.Position.Y.ToString();
                if (textBox1.Text != Cursor.Position.X.ToString() || textBox2.Text != Cursor.Position.Y.ToString())
                {
                    textBox1.Text = xx;
                    textBox2.Text = yy;
                }
            }
        }
    }
}
