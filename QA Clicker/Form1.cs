﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;
using System.IO;

namespace QA_Clicker
{
    public partial class Form1 : Form
    {
        [DllImport("user32.dll")]
        public static extern bool RegisterHotKey(IntPtr hWnd, int id, int fsModifiers, int vlc);
        [DllImport("user32.dll")]
        public static extern bool UnregisterHotKey(IntPtr hWnd, int id);
        const int WM_HOTKEY = 0x0312;
        const int MOD_ALT = 0x0001;

        bool loop = true;
        private bool _stopBot = false;
        
        public bool stopBot
        {
            get
            {
                return _stopBot;
            }
            set
            {
                _stopBot = value;
            }
        }

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Coordinates.GetCoordinates(textBox1,textBox2);
            Memory.ClearMemory();
        }

        protected override void WndProc(ref Message m)
        {
            RegisterHotKey(this.Handle, 1, MOD_ALT, (int)Keys.D);
            RegisterHotKey(this.Handle, 2, MOD_ALT, (int)Keys.S);

            if (m.Msg == WM_HOTKEY && (int)m.WParam == 1)
            {
                if (!stopBot)
                {
                    stopBot = true;
                }
            }
            if (m.Msg == WM_HOTKEY && (int)m.WParam == 2)
            {
                var x = Cursor.Position.X;
                var y = Cursor.Position.Y;
                Listbox.AddValuesToListBox("CLICK", x.ToString(), y.ToString(), listBox1);
            }
            base.WndProc(ref m);
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            if (textBox3.Text != "")
            {
                ClickProcess();
            }
            else
            {
                MessageBox.Show("The field can not be empty");
            }
        }

        private async void ClickProcess()
        {
            EnableElements(false);
            string value;
            string key;
            int lenght = listBox1.Items.Count;
            int loopCount = Convert.ToInt32(textBox5.Text);

            for (int lc = 0; lc < loopCount; lc++)
            {
                for (int i = 0; i < lenght; i++)
                {
                    if (stopBot)
                    {
                        stopBot = false;
                        lc = loopCount;
                        break;
                    }

                    Listbox.FocusCurrentlyElementOnListBox(listBox1, i);
                    value = ((KeyValuePair<String, String>)listBox1.Items[i]).Value;
                    key = ((KeyValuePair<String, String>)listBox1.Items[i]).Key;

                    switch (key)
                    {
                        case "CLICK":
                        {
                            var x = int.Parse(value.Substring(0, value.IndexOf(",")));
                            var y = int.Parse(value.Substring(value.IndexOf(",") + 2));
                            WindowsMouse.LeftMouseClick(x, y);
                            break;
                        }
                        case "WAIT":
                        {
                            await Task.Delay(int.Parse(value));
                            break;
                        }
                        case "SKEYS":
                        {
                            SendKeys.Send(value);
                            break;
                        }
                    }
                }
            }
            EnableElements(true);
        }

        private void EnableElements(bool enable)
        {
            foreach (Button button in this.Controls.OfType<Button>())
            {
                button.Enabled = enable;
            }
            foreach (TextBox textBox in this.Controls.OfType<TextBox>())
            {
                textBox.Enabled = enable;
            }
        }

        private void TextBox3_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) & e.KeyChar != (char)Keys.Back;
        }

        private void Button3_Click(object sender, EventArgs e)
        {
            listBox1.Items.Clear();
        }

        private void TextBox3_TextChanged_1(object sender, EventArgs e)
        {
            if (textBox3.Text == "0" || textBox3.Text == null || textBox3.Text == "")
            {
                textBox3.Text = "1";
            }
            if (textBox3.Text != null || textBox3.Text != "")
            {
                var txt = Int32.Parse(textBox3.Text);
                if (txt > 1000000)
                {
                    textBox3.Text = "1000000";
                }
            }
        }

        private void Button4_Click(object sender, EventArgs e)
        {
            Listbox.AddValuesToListBox("WAIT", textBox3.Text, listBox1);
        }

        private void Button6_Click(object sender, EventArgs e)
        {
            this.listBox1.Items.Clear();
            OpenFileDialog Open = new OpenFileDialog();
            Open.Filter = "Text Document|*.txt|All Files|*.*";
            try
            {
                Open.ShowDialog();
                System.IO.StreamReader Import = new System.IO.StreamReader(Convert.ToString(Open.FileName));
                while (Import.Peek() >= 0)
                    listBox1.Items.Add(Convert.ToString(Import.ReadLine()));
            }
            catch (Exception ex)
            {
                MessageBox.Show(Convert.ToString(ex.Message));
                return;
            }
        }

        private void Button5_Click_1(object sender, EventArgs e)
        {
            listBox1.Items.Remove(listBox1.SelectedItem);
        }

        private void Button6_Click_1(object sender, EventArgs e)
        {
            Listbox.MoveUp(listBox1);
        }

        private void Button7_Click(object sender, EventArgs e)
        {
            Listbox.MoveDown(listBox1);
        }

        private void Button2_Click_1(object sender, EventArgs e)
        {
            Listbox.AddValuesToListBox("SKEYS", textBox4.Text, listBox1);
        }

        private void TextBox4_TextChanged(object sender, EventArgs e)
        {
            textBox4.Text = Regex.Replace(textBox4.Text, "\\[|\\]|\\)|\\(|\\{|\\}", String.Empty);
        }

        private void TextBox5_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) & e.KeyChar != (char)Keys.Back;
        }

        private void TextBox5_TextChanged(object sender, EventArgs e)
        {
            if (textBox5.Text == "0" || textBox5.Text == null || textBox5.Text == "")
            {
                textBox5.Text = "1";
            }

            if (textBox5.Text != null || textBox5.Text != "")
            {
                var txt = Int32.Parse(textBox5.Text);
                if (txt > 1000000)
                {
                    textBox5.Text = "1000000";
                }
            }
        }

        private void Button8_Click(object sender, EventArgs e)
        {
            Listbox.OnTop(listBox1);
        }

        private void Button9_Click(object sender, EventArgs e)
        {
            Listbox.OnBottom(listBox1);
        }

        private void Button10_Click(object sender, EventArgs e)
        {
            int selectedIndex = listBox1.SelectedIndex;
            if (selectedIndex != -1)
            {
                string promptValue = Prompt.ShowEditDialog("Set new value", "",listBox1);
                if (promptValue != "")
                {
                    ReplaceValue(promptValue);
                }
            }
            else
            {
                MessageBox.Show("No Items to edit");
            }
        }
                
        private void ReplaceValue(String replacevalue)
        {
            String key, value;
            int selectedIndex = listBox1.SelectedIndex;
            value = ((KeyValuePair<String, String>)listBox1.Items[selectedIndex]).Value;
            key = ((KeyValuePair<String, String>)listBox1.Items[selectedIndex]).Key;

            switch (key)
            {
                case "CLICK":
                {
                    MessageBox.Show("No Support for edit this");
                    break;
                }
                case "WAIT":
                {
                    replacevalue = Regex.Replace(replacevalue, @"[^\d]", String.Empty);
                    listBox1.Items.RemoveAt(selectedIndex);
                    listBox1.SelectedIndex = selectedIndex - 1;
                    if (replacevalue == null || replacevalue == "")
                    {
                        replacevalue = "1";
                    }
                    Listbox.AddValuesToListBox("WAIT", replacevalue, listBox1);
                    break;
                }
                case "SKEYS":
                {
                    replacevalue = Regex.Replace(replacevalue, "\\[|\\]|\\)|\\(|\\{|\\}", String.Empty);
                    listBox1.Items.RemoveAt(selectedIndex);
                    listBox1.SelectedIndex = selectedIndex - 1;
                    Listbox.AddValuesToListBox("SKEYS", replacevalue, listBox1);
                    break;
                }
                default:
                {
                    MessageBox.Show("No Items to edit");
                    break;
                }
            }      
        }

        private void SaveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Save();
        }

        private void ExitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        private void LoadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Loading();
        }
    }
}