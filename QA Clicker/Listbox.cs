﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QA_Clicker
{
    static public class Listbox
    {
        static public void AddValuesToListBox(String key, String x, String y, ListBox listBox)
        {
            listBox.Items.Insert(listBox.SelectedIndex + 1, new KeyValuePair<String, String>(key, x + ", " + y));
            FocusCurrentlyElementOnListBox(listBox, listBox.SelectedIndex + 1);
        }
        static public void AddValuesToListBox(String key, String value, ListBox listBox)
        {
            listBox.Items.Insert(listBox.SelectedIndex + 1, new KeyValuePair<String, String>(key, value));
            FocusCurrentlyElementOnListBox(listBox, listBox.SelectedIndex + 1);
        }
        static public void FocusCurrentlyElementOnListBox(ListBox listbox, int i)
        {
            listbox.Focus();
            listbox.SelectedIndex = i;
        }
        static public void MoveUp(ListBox myListBox)
        {
            int selectedIndex = myListBox.SelectedIndex;
            if (selectedIndex > 0)
            {
                myListBox.Items.Insert(selectedIndex - 1, myListBox.Items[selectedIndex]);
                myListBox.Items.RemoveAt(selectedIndex + 1);
                myListBox.SelectedIndex = selectedIndex - 1;
            }
        }

        static public void MoveDown(ListBox myListBox)
        {
            int selectedIndex = myListBox.SelectedIndex;
            if (selectedIndex < myListBox.Items.Count - 1 & selectedIndex != -1)
            {
                myListBox.Items.Insert(selectedIndex + 2, myListBox.Items[selectedIndex]);
                myListBox.Items.RemoveAt(selectedIndex);
                myListBox.SelectedIndex = selectedIndex + 1;
            }
        }

        static public void OnTop(ListBox myListBox)
        {
            int selectedIndex = myListBox.SelectedIndex;
            while (selectedIndex > 0)
            {
                myListBox.Items.Insert(selectedIndex - 1, myListBox.Items[selectedIndex]);
                myListBox.Items.RemoveAt(selectedIndex + 1);
                myListBox.SelectedIndex = selectedIndex - 1;
                selectedIndex = myListBox.SelectedIndex;
            }
        }
        static public void OnBottom(ListBox myListBox)
        {
            int selectedIndex = myListBox.SelectedIndex;
            while (selectedIndex < myListBox.Items.Count - 1 & selectedIndex != -1)
            {
                myListBox.Items.Insert(selectedIndex + 2, myListBox.Items[selectedIndex]);
                myListBox.Items.RemoveAt(selectedIndex);
                myListBox.SelectedIndex = selectedIndex + 1;
                selectedIndex = myListBox.SelectedIndex;
            }
        }
    }
}
