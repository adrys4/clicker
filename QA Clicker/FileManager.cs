﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QA_Clicker
{
    public class FileManager
    {
        public void Save()
        {
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            saveFileDialog1.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
            saveFileDialog1.RestoreDirectory = true;

            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                foreach (KeyValuePair<string, string> item in listBox1.Items)
                {
                    File.AppendAllText(saveFileDialog1.FileName, item.Key + "|" + item.Value + Environment.NewLine);
                }
                MessageBox.Show("Scenario Saved!");
            }
        }

        public void Loading()
        {
            //Pass the file path and file name to the StreamReader constructor
            DialogResult result = openFileDialog1.ShowDialog(); // Show the dialog.
            string line = string.Empty;
            if (result == DialogResult.OK) // Test result.
            {
                string file = openFileDialog1.FileName;
                System.IO.StreamReader sr = new System.IO.StreamReader(openFileDialog1.FileName);
                try
                {
                    line = sr.ReadLine();
                    while (line != null)
                    {
                        var load1 = line.Remove(line.IndexOf("|"));
                        var load2 = line.Substring(line.IndexOf("|") + 1);
                        var newLine = new KeyValuePair<String, String>(load1, load2);
                        this.listBox1.Items.Add(newLine);
                        //Read the next line
                        line = sr.ReadLine();
                    }
                }
                catch (IOException io)
                {
                    MessageBox.Show(Convert.ToString(io.Message));
                }
            }
        }
    }
}
