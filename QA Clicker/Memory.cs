﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QA_Clicker
{
    public static class Memory
    {
        public static async void ClearMemory()
        {
            while (true)
            {
                await Task.Delay(200);
                GC.Collect();
                GC.WaitForPendingFinalizers();
            }
        }
    }
}
